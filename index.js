const { createServer } = require("http");
const { html, json } = require("./data");

const requestListener = function (req, res) {
  if (req.url == "/html") {
//GET /html - Should return the following HTML content. Note when opened in the browser it should display the HTML page and not the HTML code.

    res.writeHead(200, { "Content-Type": "text/html" });
    res.end(html);
  
} else if (req.url == "/json") {
  // GET /json - Should return the following JSON string
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(json);
 
} else if (req.url == "/uuid") {
 //GET /uuid - Should return a UUID4. For example:
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end('{"uuid":"14d96bb1-5d53-472f-a96e-b3a1fa82addd"}');

} else if (req.url.includes("/status")) {
 //GET /status/{status_code} - Should return a response with a status code as specified in the request.
    let statuscode = req.url.match(/\d\d\d/)[0];
    res.writeHead(statuscode, { "Content-Type": "text/plain" });
    res.end(` ${statuscode} response code  is sent `);

} else if (req.url.includes("/delay")) {

// GET /delay/{delay_in_seconds} - Should return a success response but after the specified delay in the request. 
// For example: If the request sent is GET /delay/3, then the server should wait for 3 seconds and only then send a response with 200 status code.
    let delay = req.url.match(/\d/)[0];
    setTimeout(() => {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end(`response was delayed for  ${delay}  secs`);
    }, parseInt(delay) * 1000);

} else {
    //  reqested page was not found
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.end(` ${req.url} reqested page was not found`);

}
};

const server = createServer(requestListener);

server.listen(8000);
